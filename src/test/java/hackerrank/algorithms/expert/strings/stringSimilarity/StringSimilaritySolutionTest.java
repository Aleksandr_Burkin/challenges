package hackerrank.algorithms.expert.strings.stringSimilarity;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringSimilaritySolutionTest {

    @Test
    public void test_1() {
        String s = "ababaa";

        StringSimilaritySolution.stringSimilarity(s);
        assertEquals(11, StringSimilaritySolution.stringSimilarity(s));
    }

    @Test
    public void test_2() {
        String s = "aa";

        assertEquals(3, StringSimilaritySolution.stringSimilarity(s));
    }

    @Test
    public void test_3() {
        String s = "aacbdbaaeaa";

        assertEquals(18, StringSimilaritySolution.stringSimilarity(s));
    }

    @Test
    public void test_4() {
        String s = "daaeddceddaccbebceccdacc";

        assertEquals(31, StringSimilaritySolution.stringSimilarity(s));
    }

    @Test
    public void test_5() {
        String s = "baeeddbaeebbaeebcdedade";

        assertEquals(33, StringSimilaritySolution.stringSimilarity(s));
    }

    @Test
    public void test_6() {
        String s = "ceebeedcdcadbbcaeeddabcecaeabdceebcdcaabadebbedaddaabdedaadcdadeeadbdae";

        assertEquals(84, StringSimilaritySolution.stringSimilarity(s));
    }

    @Test
    public void test_7() {
        String s = "bdbabdaceadeaceacdbdbaeeaacdaeddccdaadcbddaaacdeabeeddaeeecbdceaeeaceddebbcd";

        assertEquals(91, StringSimilaritySolution.stringSimilarity(s));
    }

    @Test
    public void test_8() {
        String s = "c";

        assertEquals(1, StringSimilaritySolution.stringSimilarity(s));
    }

    @Test
    public void test_9() {
        String s = "cbbcbabaeedeadabbcaedacecceeeecdddbebeccbabbbcdcbadecaabc";

        assertEquals(72, StringSimilaritySolution.stringSimilarity(s));
    }

    @Test
    public void test_10() {
        String s = "ddcebdcaebcdeaadbedacddcbaeccdacbcbccaedeecdeebcdeeadcebcbabe";

        assertEquals(75, StringSimilaritySolution.stringSimilarity(s));
    }

    @Test
    public void test_11() {
        String s = "aaeecedbdeaeaeaeccdeccbdbcdedbaabbeaaebbebabccbeebbbeceabaabdcbedaeaedbabbceeaaabbccadddaae";

        assertEquals(120, StringSimilaritySolution.stringSimilarity(s));
    }

    @Test
    public void test_12() {
        String s = "aeccecceeeacdeddcecddebbebcebcddccbbaacbbcd";

        assertEquals(46, StringSimilaritySolution.stringSimilarity(s));
    }
}
