package hackerrank.algorithms.expert.strings.palindromicBorder;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PalindromicBorderSolutionTest {

    @Test
    public void test_1() {
        String s = "aabaaabaa";
        int result = PalindromicBorderSolution.palindromicBorder(s);

        assertEquals(30, result);
    }

    @Test
    public void test_2() {
        String s = "ghghghg";
        int result = PalindromicBorderSolution.palindromicBorder(s);

        assertEquals(14, result);
    }

    @Test
    public void test_3() {
        String s = "ababa";
        int result = PalindromicBorderSolution.palindromicBorder(s);

        assertEquals(5, result);
    }

    @Test
    public void test_4() {
        String s = "abcacb";
        int result = PalindromicBorderSolution.palindromicBorder(s);

        assertEquals(3, result);
    }

    @Test
    public void test_5() {
        String s = "aaaa";
        int result = PalindromicBorderSolution.palindromicBorder(s);

        assertEquals(10, result);
    }
    @Test
    public void test_6() {
        String s = "aaaaaaaaaa";
        int result = PalindromicBorderSolution.palindromicBorder(s);

        assertEquals(165, result);
    }

    @Test
    public void test_7() {
        String s = "aabaa";
        int result = PalindromicBorderSolution.palindromicBorder(s);

        assertEquals(7, result);
    }

    @Test
    public void test_8() {
        String s = "caafaafaafaac";
        int result = PalindromicBorderSolution.palindromicBorder(s);

        assertEquals(47, result);
    }

    @Test
    public void test_performance_1() {
        String s = readStringFromResources("testfiles/palindromicBorder_1");

        long before = System.currentTimeMillis();
        int result = PalindromicBorderSolution.palindromicBorder(s);
        long timeSpent = System.currentTimeMillis() - before;

        assertEquals(165640994, result);
        assertTrue(timeSpent < 1500);
    }

    @Test
    public void test_performance_2() {
        String s = readStringFromResources("testfiles/palindromicBorder_2");

        long before = System.currentTimeMillis();
        int result = PalindromicBorderSolution.palindromicBorder(s);
        long timeSpent = System.currentTimeMillis() - before;

        assertEquals(147139817, result);
        assertTrue(timeSpent < 1500);
    }

    @Test
    public void test_performance_3() {
        String s = readStringFromResources("testfiles/palindromicBorder_3");

        long before = System.currentTimeMillis();
        int result = PalindromicBorderSolution.palindromicBorder(s);
        long timeSpent = System.currentTimeMillis() - before;

        assertEquals(665483338, result);
        assertTrue(timeSpent < 1500);
    }

    private String readStringFromResources(String path) {
        String s = null;

        try {
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            InputStream is = loader.getResourceAsStream(path);
            InputStreamReader isReader = new InputStreamReader(is, StandardCharsets.US_ASCII);
            BufferedReader reader = new BufferedReader(isReader);
            s = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return s;
    }
}
