package hackerrank.algorithms.expert.strings.palindromicBorder;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertArrayEquals;

public class PalindromicHashArrayTest {

    @Test
    public void test_1() {
        //  0  1  2  3  4  5  6  7  8  9  10 11 12
        //  c  a  a  f  a  a  f  a  a  f  a  a  c
        String s = "caafaafaafaac";
        PalindromicHashArray hashArray = new PalindromicHashArray(s);

        hashArray.hashPalindrome(0, 0);
        hashArray.hashPalindrome(1, 1);
        hashArray.hashPalindrome(1, 2);
        hashArray.hashPalindrome(2, 2);
        hashArray.hashPalindrome(3, 3);
        hashArray.hashPalindrome(2, 4);
        hashArray.hashPalindrome(4, 4);
        hashArray.hashPalindrome(1, 5);
        hashArray.hashPalindrome(4, 5);
        hashArray.hashPalindrome(5, 5);
        hashArray.hashPalindrome(3, 6);
        hashArray.hashPalindrome(6, 6);
        hashArray.hashPalindrome(2, 7);
        hashArray.hashPalindrome(5, 7);
        hashArray.hashPalindrome(7, 7);
        hashArray.hashPalindrome(1, 8);
        hashArray.hashPalindrome(4, 8);
        hashArray.hashPalindrome(7, 8);
        hashArray.hashPalindrome(8, 8);
        hashArray.hashPalindrome(3, 9);
        hashArray.hashPalindrome(6, 9);
        hashArray.hashPalindrome(9, 9);
        hashArray.hashPalindrome(2, 10);
        hashArray.hashPalindrome(5, 10);
        hashArray.hashPalindrome(8, 10);
        hashArray.hashPalindrome(10, 10);
        hashArray.hashPalindrome(1, 11);
        hashArray.hashPalindrome(4, 11);
        hashArray.hashPalindrome(7, 11);
        hashArray.hashPalindrome(10, 11);
        hashArray.hashPalindrome(11, 11);
        hashArray.hashPalindrome(0, 12);
        hashArray.hashPalindrome(12, 12);

        ArrayList<Integer> list = hashArray.getPalindromesCount();

        int[] array = new int[list.size()];
        for (int i = 0; i < array.length; i++) {
            array[i] = list.get(i);
        }

        assertArrayEquals(new int[] {1, 1, 2, 2, 8, 2, 1, 1, 2, 4, 3, 3, 3}, array);
    }
}
