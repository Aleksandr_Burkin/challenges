package hackerrank.algorithms.advanced.strings.buildPalindrome;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class PalindromicTreeTest {

    @Test
    public void test_1() {
        String s = "jbbhaaaabbfaaaa";
        int[] array = PalindromicTree.buildTree(s).getPalindromicArray();
        int[] invArray = PalindromicTree.buildTreeInverse(s).getPalindromicArray();
        String[] pals = PalindromicTree.buildTree(s).palindromes();

        assertArrayEquals(new int[] {1, 1, 2, 1, 1, 2, 3, 4, 1, 2, 1, 1, 2, 3, 4}, array);
        assertArrayEquals(new int[] {1, 2, 1, 1, 4, 3, 2, 1, 2, 1, 1, 4, 3, 2, 1}, invArray);
        assertArrayEquals(new String[] {"j", "b", "bb", "h", "a", "aa", "aaa", "aaaa", "f"}, pals);
    }

    @Test
    public void test_2() {
        String s = "aaddccddaafffabcdefedcba";
        int[] array = PalindromicTree.buildTree(s).getPalindromicArray();
        int[] invArray = PalindromicTree.buildTreeInverse(s).getPalindromicArray();
        String[] pals = PalindromicTree.buildTree(s).palindromes();

        assertArrayEquals(new int[] {1, 2, 1, 2, 1, 2, 4, 6, 8, 10, 1, 2, 3, 5, 1, 1, 1, 1, 1, 3, 5, 7, 9, 11}, array);
        assertArrayEquals(new int[] {10, 8, 6, 4, 2, 1, 2, 1, 2, 5, 3, 2, 1, 11, 9, 7, 5, 3, 1, 1, 1, 1, 1, 1}, invArray);
        assertArrayEquals(new String[] {"a", "aa", "d", "dd", "c", "cc", "dccd", "ddccdd", "addccdda", "aaddccddaa", "f", "ff", "fff", "afffa", "b", "e", "efe", "defed", "cdefedc", "bcdefedcb", "abcdefedcba"}, pals);
    }

    @Test
    public void test_3() {
        String s = "fffffffff";
        int[] array = PalindromicTree.buildTree(s).getPalindromicArray();
        int[] invArray = PalindromicTree.buildTreeInverse(s).getPalindromicArray();
        String[] pals = PalindromicTree.buildTree(s).palindromes();

        assertArrayEquals(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9}, array);
        assertArrayEquals(new int[] {9, 8, 7, 6, 5, 4, 3, 2, 1}, invArray);
        assertArrayEquals(new String[] {"f", "ff", "fff", "ffff", "fffff", "ffffff", "fffffff", "ffffffff", "fffffffff"}, pals);
    }

    @Test
    public void test_4() {
        String s = "bac";
        int[] array = PalindromicTree.buildTree(s).getPalindromicArray();
        int[] invArray = PalindromicTree.buildTreeInverse(s).getPalindromicArray();
        String[] pals = PalindromicTree.buildTree(s).palindromes();

        assertArrayEquals(new int[] {1, 1, 1}, array);
        assertArrayEquals(new int[] {1, 1, 1}, invArray);
        assertArrayEquals(new String[] {"b", "a", "c"}, pals);
    }

    @Test
    public void test_5() {
        String s = "aaa";
        int[] array = PalindromicTree.buildTree(s).getPalindromicArray();
        int[] invArray = PalindromicTree.buildTreeInverse(s).getPalindromicArray();
        String[] pals = PalindromicTree.buildTree(s).palindromes();

        assertArrayEquals(new int[] {1, 2, 3}, array);
        assertArrayEquals(new int[] {3, 2, 1}, invArray);
        assertArrayEquals(new String[] {"a", "aa", "aaa"}, pals);
    }

    @Test
    public void test_6() {
        String s = "dfhfd";
        int[] array = PalindromicTree.buildTree(s).getPalindromicArray();
        int[] invArray = PalindromicTree.buildTreeInverse(s).getPalindromicArray();
        String[] pals = PalindromicTree.buildTree(s).palindromes();

        assertArrayEquals(new int[] {1, 1, 1, 3, 5}, array);
        assertArrayEquals(new int[] {5, 3, 1, 1, 1}, invArray);
        assertArrayEquals(new String[] {"d", "f", "h", "fhf", "dfhfd"}, pals);
    }

    @Test
    public void test_7() {
        String s = "jdfh";
        int[] array = PalindromicTree.buildTree(s).getPalindromicArray();
        int[] invArray = PalindromicTree.buildTreeInverse(s).getPalindromicArray();
        String[] pals = PalindromicTree.buildTree(s).palindromes();

        assertArrayEquals(new int[] {1, 1, 1, 1}, array);
        assertArrayEquals(new int[] {1, 1, 1, 1}, invArray);
        assertArrayEquals(new String[] {"j", "d", "f", "h"}, pals);
    }

    @Test
    public void test_8() {
        String s = "a";
        int[] array = PalindromicTree.buildTree(s).getPalindromicArray();
        int[] invArray = PalindromicTree.buildTreeInverse(s).getPalindromicArray();
        String[] pals = PalindromicTree.buildTree(s).palindromes();

        assertArrayEquals(new int[] {1}, array);
        assertArrayEquals(new int[] {1}, invArray);
        assertArrayEquals(new String[] {"a"}, pals);
    }

    @Test
    public void test_9() {
        String s = "aa";
        int[] array = PalindromicTree.buildTree(s).getPalindromicArray();
        int[] invArray = PalindromicTree.buildTreeInverse(s).getPalindromicArray();
        String[] pals = PalindromicTree.buildTree(s).palindromes();

        assertArrayEquals(new int[] {1, 2}, array);
        assertArrayEquals(new int[] {2, 1}, invArray);
        assertArrayEquals(new String[] {"a", "aa"}, pals);
    }


    @Test
    public void test_10() {
        String s = "";
        int[] array = PalindromicTree.buildTree(s).getPalindromicArray();
        int[] invArray = PalindromicTree.buildTreeInverse(s).getPalindromicArray();
        String[] pals = PalindromicTree.buildTree(s).palindromes();

        assertArrayEquals(new int[] {}, array);
        assertArrayEquals(new int[] {}, invArray);
        assertArrayEquals(new String[] {}, pals);
    }

    @Test
    public void test_11() {
        String s = "yyyxxzxxyyw";
        int[] array = PalindromicTree.buildTree(s).getPalindromicArray();
        int[] invArray = PalindromicTree.buildTreeInverse(s).getPalindromicArray();
        String[] pals = PalindromicTree.buildTree(s).palindromes();

        assertArrayEquals(new int[] {1, 2, 3, 1, 2, 1, 3, 5, 7, 9, 1}, array);
        assertArrayEquals(new int[] {3, 9, 7, 5, 3, 1, 2, 1, 2, 1, 1}, invArray);
        assertArrayEquals(new String[] {"y", "yy", "yyy", "x", "xx", "z", "xzx", "xxzxx", "yxxzxxy", "yyxxzxxyy", "w"}, pals);
    }

    @Test
    public void test_12() {
        String s = "cabaca";
        int[] array = PalindromicTree.buildTree(s).getPalindromicArray();
        int[] invArray = PalindromicTree.buildTreeInverse(s).getPalindromicArray();
        String[] pals = PalindromicTree.buildTree(s).palindromes();

        assertArrayEquals(new int[] {1, 1, 1, 3, 5, 3}, array);
        assertArrayEquals(new int[] {5, 3, 1, 3, 1, 1}, invArray);
        assertArrayEquals(new String[] {"c", "a", "b", "aba", "cabac", "aca"}, pals);
    }

    @Test
    public void test_13() {
        String s = "abbcad";
        int[] array = PalindromicTree.buildTree(s).getPalindromicArray();
        int[] invArray = PalindromicTree.buildTreeInverse(s).getPalindromicArray();
        String[] pals = PalindromicTree.buildTree(s).palindromes();

        assertArrayEquals(new int[] {1, 1, 2, 1, 1, 1}, array);
        assertArrayEquals(new int[] {1, 2, 1, 1, 1, 1}, invArray);
        assertArrayEquals(new String[] {"a", "b", "bb", "c", "d"}, pals);
    }

    @Test
    public void test_14() {
        String s = "ottloictodtdtloloollllyocidyiodttoacoctcdcidcdttyoiilocltacdlydaailaiylcttilld";
        int[] array = PalindromicTree.buildTree(s).getPalindromicArray();
        int[] invArray = PalindromicTree.buildTreeInverse(s).getPalindromicArray();
        String[] pals = PalindromicTree.buildTree(s).palindromes();

        assertArrayEquals(new int[] {1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 3, 3, 1, 1, 3, 3, 2, 4, 2, 3, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 3, 1, 3, 1, 3, 1, 1, 1, 3, 1, 2, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2, 1}, array);
        assertArrayEquals(new int[] {1, 2, 1, 1, 1, 1, 1, 1, 1, 3, 3, 1, 1, 3, 3, 4, 2, 1, 4, 3, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 3, 1, 3, 1, 3, 1, 1, 1, 3, 1, 1, 2, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2, 1, 1}, invArray);
        assertArrayEquals(new String[] {"o", "t", "tt", "l", "i", "c", "d", "dtd", "tdt", "lol", "olo", "oo", "lool", "ll", "lll", "llll", "y", "a", "coc", "ctc", "cdc", "dcd", "ii", "aa"}, pals);
    }

    @Test
    public void test_15() {
        String s = "dczatfarqdkelalxzxillkfdvpfpxabqlngdscrentzamztvvcvrtcm";
        int[] array = PalindromicTree.buildTree(s).getPalindromicArray();
        int[] invArray = PalindromicTree.buildTreeInverse(s).getPalindromicArray();
        String[] pals = PalindromicTree.buildTree(s).palindromes();

        assertArrayEquals(new int[] {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 2, 1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 3, 1, 1, 1, 1}, array);
        assertArrayEquals(new int[] {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 2, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 3, 1, 1, 1, 1, 1, 1}, invArray);
        assertArrayEquals(new String[] {"d", "c", "z", "a", "t", "f", "r", "q", "k", "e", "l", "lal", "x", "xzx", "i", "ll", "v", "p", "pfp", "b", "n", "g", "s", "m", "vv", "vcv"}, pals);
    }
}
