package hackerrank.algorithms.advanced.search.similarPair;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SimilarPairSolutionTest {

    @Test
    public void test_1() {
        int n = 5;
        int k = 2;
        int[][] edges = new int[][] {{3, 2}, {3, 1}, {1, 4}, {1, 5}};

        assertEquals(4, SimilarPairSolution.similarPair(n, k, edges));
    }

    @Test
    public void test_2() {
        int n = 6;
        int k = 3;
        int[][] edges = new int[][] {{1, 2}, {1, 3}, {3, 4}, {3, 5}, {3, 6}};

        assertEquals(6, SimilarPairSolution.similarPair(n, k, edges));
    }
}
