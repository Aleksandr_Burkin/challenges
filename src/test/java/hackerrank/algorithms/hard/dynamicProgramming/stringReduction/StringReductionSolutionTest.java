package hackerrank.algorithms.hard.dynamicProgramming.stringReduction;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringReductionSolutionTest {

    @Test
    public void test_1() {
        String s = "cab";

        assertEquals(2, StringReductionSolution.stringReduction(s));
    }

    @Test
    public void test_2() {
        String s = "bcab";

        assertEquals(1, StringReductionSolution.stringReduction(s));
    }

    @Test
    public void test_3() {
        String s = "ccccc";

        assertEquals(5, StringReductionSolution.stringReduction(s));
    }

    @Test
    public void test_4() {
        String s = "abbbba";

        assertEquals(2, StringReductionSolution.stringReduction(s));
    }

    @Test
    public void test_5() {
        String s = "bbbba";

        assertEquals(1, StringReductionSolution.stringReduction(s));
    }

    @Test
    public void test_6() {
        String s = "abbbbaa";

        assertEquals(1, StringReductionSolution.stringReduction(s));
    }
}
