package hackerrank.algorithms.hard.dynamicProgramming.hexagonalGrid;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HexagonalGridSolutionTest {

    @Test
    public void test_1() {
        String a = "010000";
        String b = "000010";

        assertEquals("YES", HexagonalGridSolution.hexagonalGrid(a, b));
    }

    @Test
    public void test_2() {
        String a = "00";
        String b = "00";

        assertEquals("YES", HexagonalGridSolution.hexagonalGrid(a, b));
    }

    @Test
    public void test_3() {
        String a = "00";
        String b = "10";

        assertEquals("NO", HexagonalGridSolution.hexagonalGrid(a, b));
    }

    @Test
    public void test_4() {
        String a = "00";
        String b = "01";

        assertEquals("NO", HexagonalGridSolution.hexagonalGrid(a, b));
    }

    @Test
    public void test_5() {
        String a = "00";
        String b = "11";

        assertEquals("YES", HexagonalGridSolution.hexagonalGrid(a, b));
    }

    @Test
    public void test_6() {
        String a = "10";
        String b = "00";

        assertEquals("NO", HexagonalGridSolution.hexagonalGrid(a, b));
    }
}
