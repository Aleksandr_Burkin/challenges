package hackerrank.algorithms.hard.implementation.matrixLayerRotation;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class MatrixLayerRotationSolutionTest {

    @Test
    public void test_1() {
        int[][] matrix = {
                {1, 2, 3, 4},
                {12, 1, 2, 5},
                {11, 4, 3, 6},
                {10, 9, 8, 7}};

        MatrixLayerRotationSolution.matrixRotation(matrix, 2);

        assertArrayEquals(new int[][] {
                {3, 4, 5, 6},
                {2, 3, 4, 7},
                {1, 2, 1, 8},
                {12, 11, 10, 9}}, matrix);
    }

    @Test
    public void test_2() {
        int[][] matrix = {
                {1, 2, 3, 4},
                {14, 1, 2, 5},
                {13, 6, 3, 6},
                {12, 5, 4, 7},
                {11, 10, 9, 8}};

        MatrixLayerRotationSolution.matrixRotation(matrix, 3);

        assertArrayEquals(new int[][] {
                {4, 5, 6, 7},
                {3, 4, 5, 8},
                {2, 3, 6, 9},
                {1, 2, 1, 10},
                {14, 13, 12, 11}}, matrix);
    }

    @Test
    public void test_3() {
        int[][] matrix = {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16}};

        MatrixLayerRotationSolution.matrixRotation(matrix, 2);

        assertArrayEquals(new int[][] {
                {3, 4, 8, 12},
                {2, 11, 10, 16},
                {1, 7, 6, 15},
                {5, 9, 13, 14}}, matrix);
    }

    @Test
    public void test_4() {
        int[][] matrix = {
                {1, 2, 3, 4},
                {7, 8, 9, 10},
                {13, 14, 15, 16},
                {19, 20, 21, 22},
                {25, 26, 27, 28}};

        MatrixLayerRotationSolution.matrixRotation(matrix, 7);

        assertArrayEquals(new int[][] {
                {28, 27, 26, 25},
                {22, 9, 15, 19},
                {16, 8, 21, 13},
                {10, 14, 20, 7},
                {4, 3, 2, 1}}, matrix);
    }

    @Test
    public void test_5() {
        int[][] matrix = {
                {1, 1},
                {1, 1}};

        MatrixLayerRotationSolution.matrixRotation(matrix, 3);

        assertArrayEquals(new int[][] {
                {1, 1},
                {1, 1}}, matrix);
    }
}
