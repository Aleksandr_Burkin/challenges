package hackerrank.algorithms.medium.dynamicProgramming.redJohnIsBack;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RedJohnIsBackSolutionTest {

    @Test
    public void test_1() {
        assertEquals(0, RedJohnIsBackSolution.redJohn(1));
    }

    @Test
    public void test_2() {
        assertEquals(0, RedJohnIsBackSolution.redJohn(2));
    }

    @Test
    public void test_3() {
        assertEquals(0, RedJohnIsBackSolution.redJohn(3));
    }

    @Test
    public void test_4() {
        assertEquals(1, RedJohnIsBackSolution.redJohn(4));
    }

    @Test
    public void test_5() {
        assertEquals(2, RedJohnIsBackSolution.redJohn(5));
    }

    @Test
    public void test_6() {
        assertEquals(2, RedJohnIsBackSolution.redJohn(6));
    }

    @Test
    public void test_7() {
        assertEquals(3, RedJohnIsBackSolution.redJohn(7));
    }

    @Test
    public void test_8() {
        assertEquals(4, RedJohnIsBackSolution.redJohn(8));
    }

    @Test
    public void test_9() {
        assertEquals(4, RedJohnIsBackSolution.redJohn(9));
    }

    @Test
    public void test_10() {
        assertEquals(19385, RedJohnIsBackSolution.redJohn(40));
    }
}
