package hackerrank.algorithms.medium.dynamicProgramming.samAndSubstrings;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SamAndSubstringsSolutionTest {

    @Test
    public void test_1() {
        String s = "16";

        assertEquals(23, SamAndSubstringsSolution.substrings(s));
    }

    @Test
    public void test_2() {
        String s = "123";

        assertEquals(164, SamAndSubstringsSolution.substrings(s));
    }

    @Test
    public void test_3() {
        String s = "1234";

        assertEquals(1670, SamAndSubstringsSolution.substrings(s));
    }

    @Test
    public void test_4() {
        String s = "200000";

        assertEquals(222222, SamAndSubstringsSolution.substrings(s));
    }

    @Test
    public void test_5() {
        String s = "199999";

        assertEquals(345651, SamAndSubstringsSolution.substrings(s));
    }
}
