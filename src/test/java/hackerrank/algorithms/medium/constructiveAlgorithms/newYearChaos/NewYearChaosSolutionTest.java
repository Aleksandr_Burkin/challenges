package hackerrank.algorithms.medium.constructiveAlgorithms.newYearChaos;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NewYearChaosSolutionTest {

    @Test
    public void test_1() {
        int[] a = {3, 2, 1, 5, 4};

        assertEquals("4", NewYearChaosSolution.minimumBribes(a));
    }

    @Test
    public void test_2() {
        int[] a = {3, 2, 5, 1, 4};

        assertEquals("5", NewYearChaosSolution.minimumBribes(a));
    }

    @Test
    public void test_3() {
        int[] a = {3, 4, 2, 1, 5};

        assertEquals("5", NewYearChaosSolution.minimumBribes(a));
    }

    @Test
    public void test_4() {
        int[] a = {3, 5, 2, 1, 4};

        assertEquals("Too chaotic", NewYearChaosSolution.minimumBribes(a));
    }
}
