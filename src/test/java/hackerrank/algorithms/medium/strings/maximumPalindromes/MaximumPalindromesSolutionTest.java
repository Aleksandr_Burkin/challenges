package hackerrank.algorithms.medium.strings.maximumPalindromes;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MaximumPalindromesSolutionTest {

    @Test
    public void test_1() {
        String s = "week";

        MaximumPalindromesSolution.initialize(s);
        assertEquals(2, MaximumPalindromesSolution.answerQuery(1, 4));
    }

    @Test
    public void test_2() {
        String s = "abab";

        MaximumPalindromesSolution.initialize(s);
        assertEquals(2, MaximumPalindromesSolution.answerQuery(1, 4));
    }

    @Test
    public void test_3() {
        String s = "aabbcc";

        MaximumPalindromesSolution.initialize(s);
        assertEquals(6, MaximumPalindromesSolution.answerQuery(1, 6));
    }

    @Test
    public void test_4() {
        String s = "aabbccd";

        MaximumPalindromesSolution.initialize(s);
        assertEquals(6, MaximumPalindromesSolution.answerQuery(1, 7));
    }

    @Test
    public void test_5() {
        String s = "aabbccdf";

        MaximumPalindromesSolution.initialize(s);
        assertEquals(12, MaximumPalindromesSolution.answerQuery(1, 8));
    }

    @Test
    public void test_6() {
        String s = "madamimadam";

        MaximumPalindromesSolution.initialize(s);
        assertEquals(2, MaximumPalindromesSolution.answerQuery(4, 7));
    }

    @Test
    public void test_7() {
        String s = "wldsfubcsxrryqpqyqqxrlffumtuwymbybnpemdiwyqz";

        MaximumPalindromesSolution.initialize(s);
        assertEquals(60480, MaximumPalindromesSolution.answerQuery(15, 40));
    }
}
