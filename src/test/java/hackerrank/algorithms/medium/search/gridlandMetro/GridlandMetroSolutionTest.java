package hackerrank.algorithms.medium.search.gridlandMetro;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;

public class GridlandMetroSolutionTest {

    @Test
    public void test_1() {
        int r = 4;
        int c = 4;
        int k = 3;
        int[][] track = new int[][] {{2, 2, 3}, {3, 1, 4}, {4, 4, 4}};

        assertEquals(9, GridlandMetroSolution.gridlandMetro(r, c, k, track));
    }

    @Test
    public void test_2() {
        int r = 2;
        int c = 9;
        int k = 3;
        int[][] track = new int[][] {{2, 1, 5}, {2, 2, 4}, {2, 8, 8}};

        assertEquals(12, GridlandMetroSolution.gridlandMetro(r, c, k, track));
    }

    @Test
    public void test_performance_1() {
        String[] params = null;
        String[] strTrack = null;

        try {
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            InputStream is = loader.getResourceAsStream("testfiles/gridlandMetroSolution");
            InputStreamReader isReader = new InputStreamReader(is, StandardCharsets.US_ASCII);
            BufferedReader reader = new BufferedReader(isReader);
            params = reader.readLine().split(" ");
            strTrack = new String[Integer.parseInt(params[2])];
            for (int i = 0; i < strTrack.length; i++) {
                strTrack[i] = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        int r = Integer.parseInt(params[0]);
        int c = Integer.parseInt(params[1]);
        int k = Integer.parseInt(params[2]);
        int[][] track = new int[k][3];

        for (int i = 0; i < strTrack.length; i++) {
            String[] a = strTrack[i].split(" ");
            track[i][0] = Integer.parseInt(a[0]);
            track[i][1] = Integer.parseInt(a[1]);
            track[i][2] = Integer.parseInt(a[2]);
        }

        assertEquals(343959391703854850L, GridlandMetroSolution.gridlandMetro(r, c, k, track));
    }
}
