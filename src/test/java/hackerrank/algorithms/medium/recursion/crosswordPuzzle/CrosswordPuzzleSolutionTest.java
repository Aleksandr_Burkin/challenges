package hackerrank.algorithms.medium.recursion.crosswordPuzzle;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class CrosswordPuzzleSolutionTest {

    @Test
    public void test_1() {
        String[] crossword = {
                "++++++++++",
                "+------+++",
                "+++-++++++",
                "+++-++++++",
                "+++-----++",
                "+++-++-+++",
                "++++++-+++",
                "++++++-+++",
                "++++++-+++",
                "++++++++++"
        };

        String words = "POLAND;LHASA;SPAIN;INDIA";

        String[] result = {
                "++++++++++",
                "+POLAND+++",
                "+++H++++++",
                "+++A++++++",
                "+++SPAIN++",
                "+++A++N+++",
                "++++++D+++",
                "++++++I+++",
                "++++++A+++",
                "++++++++++"
        };

        assertArrayEquals(result, CrosswordPuzzleSolution.crosswordPuzzle(crossword, words));
    }

    @Test
    public void test_2() {
        String[] crossword = {
                "+-++++++++",
                "+-++++++++",
                "+-++++++++",
                "+-----++++",
                "+-+++-++++",
                "+-+++-++++",
                "+++++-++++",
                "++------++",
                "+++++-++++",
                "+++++-++++"
        };

        String words = "LONDON;DELHI;ICELAND;ANKARA";

        String[] result = {
                "+L++++++++",
                "+O++++++++",
                "+N++++++++",
                "+DELHI++++",
                "+O+++C++++",
                "+N+++E++++",
                "+++++L++++",
                "++ANKARA++",
                "+++++N++++",
                "+++++D++++"
        };

        assertArrayEquals(result, CrosswordPuzzleSolution.crosswordPuzzle(crossword, words));
    }

    @Test
    public void test_3() {
        String[] crossword = {
                "+-++++++++",
                "+-++++++++",
                "+-------++",
                "+-++++++++",
                "+-++++++++",
                "+------+++",
                "+-+++-++++",
                "+++++-++++",
                "+++++-++++",
                "++++++++++"
        };

        String words = "AGRA;NORWAY;ENGLAND;GWALIOR";

        String[] result = {
                "+E++++++++",
                "+N++++++++",
                "+GWALIOR++",
                "+L++++++++",
                "+A++++++++",
                "+NORWAY+++",
                "+D+++G++++",
                "+++++R++++",
                "+++++A++++",
                "++++++++++"
        };

        assertArrayEquals(result, CrosswordPuzzleSolution.crosswordPuzzle(crossword, words));
    }

    @Test
    public void test_4() {
        String[] crossword = {
                "++++++-+++",
                "++------++",
                "++++++-+++",
                "++++++-+++",
                "+++------+",
                "++++++-+-+",
                "++++++-+-+",
                "++++++++-+",
                "++++++++-+",
                "++++++++-+"
        };

        String words = "ICELAND;MEXICO;PANAMA;ALMATY";

        String[] result = {
                "++++++I+++",
                "++MEXICO++",
                "++++++E+++",
                "++++++L+++",
                "+++PANAMA+",
                "++++++N+L+",
                "++++++D+M+",
                "++++++++A+",
                "++++++++T+",
                "++++++++Y+"
        };

        assertArrayEquals(result, CrosswordPuzzleSolution.crosswordPuzzle(crossword, words));
    }
}
