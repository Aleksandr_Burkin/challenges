package hackerrank.dataStructures.medium.stacks.gameOfTwoStacks;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GameOfTwoStacksSolutionTest {

    @Test
    public void test_1() {
        int x = 10;
        int[] a = new int[] {4, 2, 4, 6, 1};
        int[] b = new int[] {2, 1, 8, 5};

        assertEquals(4, GameOfTwoStacksSolution.twoStacks(x, a, b));
    }

    @Test
    public void test_2() {
        int x = 19;
        int[] a = new int[] {14, 0, 15, 12, 15, 6, 15, 0, 18, 19, 16, 1, 3, 9, 5, 19, 0, 10, 10, 2, 14, 12, 1, 4, 6, 6, 10, 16, 7, 2, 14, 2};
        int[] b = new int[] {2, 2, 6, 9, 0, 1, 1, 18, 12, 17, 11, 16, 18, 8, 7, 18, 19, 17, 13, 13, 2, 14, 10, 8, 0, 0, 4, 0, 2, 11, 2, 16};

        assertEquals(5, GameOfTwoStacksSolution.twoStacks(x, a, b));
    }
}
