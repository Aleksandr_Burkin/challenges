package hackerrank.dataStructures.hard.arrays.arrayManipulation;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ArrayManipulationSolutionTest {

    @Test
    public void test_1() {
        int n = 10;
        int[][] queries = new int[][] {{1, 5, 3}, {4, 8, 7}, {6, 9, 1}};

        assertEquals(10, ArrayManipulationSolution.arrayManipulation(n, queries));
    }

    @Test
    public void test_2() {
        int n = 5;
        int[][] queries = new int[][] {{1, 2, 100}, {2, 5, 100}, {3, 4, 100}};

        assertEquals(200, ArrayManipulationSolution.arrayManipulation(n, queries));
    }

    @Test
    public void test_3() {
        int n = 10;
        int[][] queries = new int[][] {{2, 6, 8}, {3, 5, 7}, {1, 8, 1}, {5, 9, 15}};

        assertEquals(31, ArrayManipulationSolution.arrayManipulation(n, queries));
    }
}
