package hackerrank.dataStructures.hard.trie.noPrefixSet;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class NoPrefixSetSolutionTest {

    @Test
    public void test_1() {
        String[] strings = {"aab", "defgab", "abcde", "aabcde", "cedaaa", "bbbbbbbbbb", "jabjjjad"};

        assertArrayEquals(new String[] {"BAD SET", "aabcde"}, NoPrefixSetSolution.matchPrefix(strings));
    }

    @Test
    public void test_2() {
        String[] strings = {"aab", "aac", "aacghgh", "aabghgh"};

        assertArrayEquals(new String[] {"BAD SET", "aacghgh"}, NoPrefixSetSolution.matchPrefix(strings));
    }

    @Test
    public void test_3() {
        String[] strings = {"aab", "defgab", "abcde", "cedaaa", "bbbbbbbbbb", "jabjjjad"};

        assertArrayEquals(new String[] {"GOOD SET"}, NoPrefixSetSolution.matchPrefix(strings));
    }

    @Test
    public void test_4() {
        String[] strings = {
                "hgiiccfchbeadgebc", "biiga", "edchgb", "ccfdbeajaeid", "ijgbeecjbj", "bcfbbacfbfcfbhcbfjafibfhffac",
                "ebechbfhfcijcjbcehbgbdgbh", "ijbfifdbfifaidje", "acgffegiihcddcdfjhhgadfjb", "ggbdfdhaffhghbdh",
                "dcjaichjejgheiaie", "d", "jeedfch", "ahabicdffbedcbdeceed", "fehgdfhdiffhegafaaaiijceijdgbb",
                "beieebbjdgdhfjhj", "ehg", "fdhiibhcbecddgijdb", "jgcgafgjjbg", "c", "fiedahb", "bhfhjgcdbjdcjjhaebejaecdheh",
                "gbfbbhdaecdjaebadcggbhbchfjg", "jdjejjg", "dbeedfdjaghbhgdhcedcj", "decjacchhaciafafdgha", "a",
                "hcfibighgfggefghjh", "ccgcgjgaghj", "bfhjgehecgjchcgj", "bjbhcjcbbhf", "daheaggjgfdcjehidfaedjfccdafg",
                "efejicdecgfieef", "ciidfbibegfca", "jfhcdhbagchjdadcfahdii", "i", "abjfjgaghbc", "bddeejeeedjdcfcjcieceieaei",
                "cijdgbddbceheaeececeeiebafgi", "haejgebjfcfgjfifhihdbddbacefd", "bhhjbhchdiffb", "jbbdhcbgdefifhafhf",
                "ajhdeahcjjfie", "idjajdjaebfhhaacecb", "bhiehhcggjai", "bjjfjhiice", "aif", "gbbfjedbhhhjfegeeieig",
                "fefdhdaiadefifjhedaieaefc", "hgaejbhdebaacbgbgfbbcad", "heghcb", "eggadagajjgjgaihjdigihfhfbijbh",
                "jadeehcciedcbjhdeca", "ghgbhhjjgghgie", "ibhihfaeeihdffjgddcj", "hiedaegjcdai", "bjcdcafgfjdejgiafdhfed",
                "fgdgjaihdjaeefejbbijdbfabeie", "aeefgiehgjbfgidcedjhfdaaeigj", "bhbiaeihhdafgaciecb", "igicjdajjdegbceibgebedghihihh",
                "baeeeehcbffd", "ajfbfhhecgaghgfdadbfbb", "ahgaccehbgajcdfjihicihhc", "bbjhih", "a", "cdfcdejacaicgibghgddd",
                "afeffehfcfiefhcagg", "ajhebffeh", "e", "hhahehjfgcj", "ageaccdcbbcfidjfc", "gfcjahbbhcbggadcaebae", "gi",
                "edheggceegiedghhdfgabgcd", "hejdjjbfacggdccgahiai", "ffgeiadgjfgecdbaebagij", "dgaiahge", "hdbaifh",
                "gbhccajcdebcig", "ejdcbbeiiebjcagfhjfdahbif", "g", "ededbjaaigdhb", "ahhhcibdjhidbgefggdjebfcf",
                "bdigjaehfchebiedajcjdh", "fjehjgbdbaiifi", "fbgigbdcbcgffdicfcidfdafghajc", "ccajeeijhhb",
                "gaaagfacgiddfahejhbgdfcfbfeedh", "gdajaigfbjcdegeidgaccjfi", "fghechfchjbaebcghfcfbdicgaic",
                "cfhigaciaehacdjhfcgajgbhhgj", "edhjdbdjccbfihiaddij", "cbbhagjbcadegicgifgghai", "hgdcdhieji",
                "fbifgbhdhagch", "cbgcdjea", "dggjafcajhbbbaja", "bejihed", "eeahhcggaaidifdigcfjbficcfhjj"};

        assertArrayEquals(new String[] {"BAD SET", "d"}, NoPrefixSetSolution.matchPrefix(strings));
    }
}
