package hackerrank.dataStructures.hard.queues.queriesWithFixedLength;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class QueriesWithFixedLengthSolutionTest {

    @Test
    public void test_1() {
        int[] a = new int[] {33, 11, 44, 11, 55};
        int[] queries = new int[] {1, 2, 3, 4, 5};

        assertArrayEquals(new int[] {11, 33, 44, 44, 55}, QueriesWithFixedLengthSolution.solve(a, queries));
    }

    @Test
    public void test_2() {
        int[] a = new int[] {1, 2, 3, 4, 5};
        int[] queries = new int[] {1, 2, 3, 4, 5};

        assertArrayEquals(new int[] {1, 2, 3, 4, 5}, QueriesWithFixedLengthSolution.solve(a, queries));
    }
}
