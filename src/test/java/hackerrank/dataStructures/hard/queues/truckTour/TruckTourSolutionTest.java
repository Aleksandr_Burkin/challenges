package hackerrank.dataStructures.hard.queues.truckTour;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TruckTourSolutionTest {

    @Test
    public void test_1() {
        int[][] petrolpumps = new int[][] {{1, 5}, {10, 3}, {3, 4}};

        assertEquals(1, TruckTourSolution.truckTour(petrolpumps));
    }
}
