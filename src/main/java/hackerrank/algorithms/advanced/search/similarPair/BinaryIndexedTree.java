package hackerrank.algorithms.advanced.search.similarPair;

public class BinaryIndexedTree {

    private static int MAX = 100005;
    private int bit[];
    private int n;

    public BinaryIndexedTree(int n) {
        this.bit = new int[MAX];
        this.n = n;
    }

    int lowbit(int x) {
        return x & (-x);
    }

    void add(int x, int num) {
        for (int i = x; i <= n; i += lowbit(i))
            bit[i] += num;
    }

    int getSum(int x) {
        int sum = 0;
        for (int i = x; i > 0; i -= lowbit(i))
            sum += bit[i];

        return sum;
    }
}
