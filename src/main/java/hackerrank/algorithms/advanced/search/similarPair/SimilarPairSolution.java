package hackerrank.algorithms.advanced.search.similarPair;

import java.util.ArrayList;

/**
 * Hackerrank challenge.
 *
 * @see <a href="https://www.hackerrank.com/challenges/similarpair/problem">SimilarPair</a>
 *
 * @author Aleksandr Burkin
 */
public class SimilarPairSolution {

    private static int pair;

    private static class Node {
        int value;
        ArrayList<Node> children;
        boolean isVisited;

        Node(int value) {
            this.value = value;
            this.children = new ArrayList<>();
        }

        void addChild(Node child) {
            children.add(child);
        }
    }

    static int similarPair(int n, int k, int[][] edges) {
        pair = 0;
        Node root = buildTree(n, edges);

        BinaryIndexedTree bit = new BinaryIndexedTree(n);

        dfs(root, bit, k, n);

        return pair;
    }

    private static void dfs(Node current, BinaryIndexedTree bit, int k, int n) {
        pair += bit.getSum(Math.min(n, current.value + k)) - bit.getSum(Math.max(1, current.value - k) - 1);
        bit.add(current.value, 1);

        current.isVisited = true;
        for (Node child : current.children) {
            if (!child.isVisited) dfs(child, bit, k, n);
        }

        bit.add(current.value, -1);
    }

    private static Node buildTree(int n, int[][] edges) {
        //aux
        Node[] nodes = new Node[n + 1];

        for (int[] edge : edges) {
            int parent = edge[0];
            int child = edge[1];
            if (nodes[parent] == null)
                nodes[parent] = new Node(parent);
            if (nodes[child] == null)
                nodes[child] = new Node(child);

            nodes[parent].addChild(nodes[child]);
        }

        return nodes[edges[0][0]];
    }
}
