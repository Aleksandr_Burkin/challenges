package hackerrank.algorithms.advanced.strings.buildPalindrome;

/**
 * Kasai, Arimura, Arikawa, Lee, Park algorithm
 * for construction of LCP Array from Suffix Array in O(n) time
 */
public class LCP {

    static int[] kasai(String s, int[] suff) {
        int n = suff.length;
        int[] lcp = new int[n];

        int[] inv = new int[n];
        for (int i = 0; i < n; i++)
            inv[suff[i]] = i;

        int k = 0;

        for (int i = 0; i < n; i++) {
            if (inv[i] == n - 1) {
                k = 0;
                continue;
            }

            int j = suff[inv[i] + 1];

            while (i + k < n && j + k < n && s.charAt(i + k) == s.charAt(j + k))
                k++;

            lcp[inv[i]] = k;

            if (k > 0) k--;
        }

        return lcp;
    }
}
