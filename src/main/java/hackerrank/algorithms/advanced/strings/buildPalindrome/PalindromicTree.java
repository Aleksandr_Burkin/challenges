package hackerrank.algorithms.advanced.strings.buildPalindrome;

import java.util.HashMap;

/**
 * PalindromicTree and PalindromicArray.
 *
 * PalindromicArray is auxiliary data structure
 * containing the length of the longest palindromic substring
 * in each array index.
 *
 * @author Aleksandr Burkin
 */
public class PalindromicTree {

    private String str;

    private HashMap<Integer, Node> tree;

    private int current;

    //Index to store next unique palindrome
    private int next;

    private int[] palindromicArray;

    private boolean isInverse;

    private class Node {
        int low, high;

        int length;

        int[] edges = new int[26];

        int suffixEdge;
    }

    private PalindromicTree(String str, boolean isInverse) {
        this.str = str;
        this.isInverse = isInverse;

        Node imaginary = new Node();
        imaginary.length = -1;
        imaginary.suffixEdge = 1;

        Node empty = new Node();
        empty.length = 0;
        empty.suffixEdge = 1;

        tree = new HashMap<>();
        tree.put(1, imaginary);
        tree.put(2, empty);

        next = 2;
        current = 1;

        palindromicArray = new int[str.length()];
    }

    private int index(int i) {
        return str.charAt(i) - 'a';
    }

    private int searchParent(int edge, int i) {
        int parent = edge;

        int length = tree.get(parent).length;

        if (isInverse) {
            while ((str.length() - i - 1 - length < 1) || (str.charAt(i) != str.charAt(i + length + 1))) {
                parent = tree.get(parent).suffixEdge;
                length = tree.get(parent).length;
            }
        } else {
            while ((i - length < 1) || (str.charAt(i) != str.charAt(i - length - 1))) {
                parent = tree.get(parent).suffixEdge;
                length = tree.get(parent).length;
            }
        }

        return parent;
    }

    private void createPalindromeNode(int i, int parent) {
        next++;

        tree.get(parent).edges[index(i)] = next;

        Node node = tree.computeIfAbsent(next, arg -> new Node());
        node.length = tree.get(parent).length + 2;
        node.low = i - tree.get(next).length + 1;
        node.high = i;

        palindromicArray[i] = tree.get(next).length;

        if (tree.get(next).length == 1) {
            tree.get(next).suffixEdge = 2;
        } else {
            parent = searchParent(tree.get(parent).suffixEdge, i);
            tree.get(next).suffixEdge = tree.get(parent).edges[index(i)];
        }

        current = next;
    }

    private void insert(int i) {
        int parent = searchParent(current, i);

        //if palindrome found
        if (tree.get(parent).edges[index(i)] != 0) {
            current = tree.get(parent).edges[index(i)];

            int length = tree.get(current).length;

            palindromicArray[i] = Math.max(palindromicArray[i], length);
        } else {
            createPalindromeNode(i, parent);
        }
    }

    public static PalindromicTree buildTree(String str) {
        PalindromicTree pt = new PalindromicTree(str, false);

        for (int i = 0; i < pt.str.length(); i++)
            pt.insert(i);

        return pt;
    }

    public static PalindromicTree buildTreeInverse(String str) {
        PalindromicTree pt = new PalindromicTree(str, true);

        for (int i = pt.str.length() - 1; i >= 0; i--)
            pt.insert(i);

        return pt;
    }

    public int[] getPalindromicArray() {
        return palindromicArray;
    }

    public String[] palindromes() {
        String[] strings = new String[next - 2];

        for (int i = 3; i <= next; i++)
            strings[i - 3] = str.substring(tree.get(i).low, tree.get(i).high + 1);

        return strings;
    }
}
