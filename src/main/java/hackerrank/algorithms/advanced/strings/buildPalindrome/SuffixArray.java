package hackerrank.algorithms.advanced.strings.buildPalindrome;

/**
 * Karkkainen-Sanders algorithm
 * for construction of Suffix Array in O(n) time
 */
public class SuffixArray {

    static int[] constructSuffixArray(int[] source, int K) {
        int[] sa = new int[source.length];

        suffixArray(source, sa, source.length, K);

        return sa;
    }

    private static void suffixArray(int[] source, int[] SA, int n, int K) {
        int n0 = (n + 2) / 3;
        int n1 = (n + 1) / 3;
        int n2 = n / 3;
        int n02 = n0 + n2;

        int[] R = new int[n02 + 3];
        int[] SA12 = new int[n02 + 3];
        int[] R0 = new int[n0];
        int[] SA0 = new int[n0];

        int[] T = appendCharsToEnd(source);

        for (int i = 0, j = 0; i < n + (n0 - n1); i++)
            if (i % 3 != 0) R[j++] = i;

        radixPass(R, SA12, T, n02, K, 2);
        radixPass(SA12, R, T, n02, K, 1);
        radixPass(R, SA12, T, n02, K, 0);

        int name = 0, c0 = -1, c1 = -1, c2 = -1;
        for (int i = 0;  i < n02; i++) {
            if (T[SA12[i]] != c0 || T[SA12[i] + 1] != c1 || T[SA12[i] + 2] != c2) {
                name++;
                c0 = T[SA12[i]];
                c1 = T[SA12[i]+1];
                c2 = T[SA12[i]+2];
            }

            if (SA12[i] % 3 == 1) R[SA12[i]/3] = name;
            else R[SA12[i]/3 + n0] = name;
        }

        if (name < n02) {
            suffixArray(R, SA12, n02, name);
            for (int i = 0; i < n02; i++) R[SA12[i]] = i + 1;
        } else {
            for (int i = 0; i < n02; i++) SA12[R[i] - 1] = i;
        }

        for (int i = 0, j = 0; i < n02; i++)
            if (SA12[i] < n0) R0[j++] = 3 * SA12[i];

        radixPass(R0, SA0, T, n0, K, 0);

        for (int p = 0, t = n0 - n1, k = 0; k < n; k++) {
            int i = SA12[t] < n0 ? SA12[t] * 3 + 1 : (SA12[t] - n0) * 3 + 2;
            int j = SA0[p];

            if (SA12[t] < n0 ?
                    compare(T[i], R[SA12[t] + n0], T[j], R[j/3]) :
                    compare(T[i], T[i + 1], R[SA12[t] - n0 + 1], T[j], T[j + 1], R[j / 3 + n0])) {
                SA[k] = i; t++;
                if (t == n02)
                    for (k++; p < n0; p++, k++) SA[k] = SA0[p];
            } else {
                SA[k] = j; p++;
                if (p == n0)
                    for (k++; t < n02; t++, k++)
                        SA[k] = SA12[t] < n0 ? SA12[t] * 3 + 1 : (SA12[t] - n0) * 3 + 2;
            }
        }
    }

    private static boolean compare(int a1, int a2, int b1, int b2) {
        return a1 < b1 || a1 == b1 && a2 <= b2;
    }

    private static boolean compare(int a1, int a2, int a3, int b1, int b2, int b3) {
        return a1 < b1 || a1 == b1 && compare(a2, a3, b2, b3);
    }

    private static void radixPass(int[] from, int[] to, int[] r, int n, int K, int offset) {
        int[] count = new int[K + 1];

        for (int i = 0;  i < n;  i++)
            count[r[from[i] + offset]]++;

        for (int i = 0, sum = 0;  i <= K;  i++) {
            int t = count[i];
            count[i] = sum;
            sum += t;
        }

        for (int i = 0;  i < n;  i++)
            to[count[r[from[i] + offset]]++] = from[i];
    }

    private static int[] appendCharsToEnd(int[] s) {
        int[] s2;

        if (s.length % 3 == 1) s2 = new int[s.length + 3];
        else s2 = new int[s.length + 2];

        System.arraycopy(s, 0, s2, 0, s.length);

        return s2;
    }
}
