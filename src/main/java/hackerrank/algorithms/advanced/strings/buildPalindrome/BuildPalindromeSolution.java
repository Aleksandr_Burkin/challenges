package hackerrank.algorithms.advanced.strings.buildPalindrome;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Hackerrank challenge.
 *
 * @see <a href="https://www.hackerrank.com/challenges/challenging-palindromes/problem">BuildPalindrome</a>
 *
 * @author Aleksandr Burkin
 */
public class BuildPalindromeSolution {

    public static String build(String a, String b) {
        String longest_A = longestPalindrome_A_Search(a, b);
        String longest_B = longestPalindrome_B_Search(b, a);

        if (!longest_A.isEmpty() && !longest_B.isEmpty()) {
            if (longest_A.length() > longest_B.length()) {
                return longest_A;
            } else if (longest_A.length() < longest_B.length()) {
                return longest_B;
            } else {
                if (longest_A.compareTo(longest_B) < 0) return longest_A;
                else return longest_B;
            }
        } else {
            return "-1";
        }
    }

    private static String longestPalindrome_A_Search(String s1, String s2) {
        int[] subs = substringsLengthLeft(s1, s2);
        int[] pals = PalindromicTree.buildTreeInverse(s1).getPalindromicArray();

        int max = 0;

        HashMap<Integer, ArrayList<Integer>> indexes = new HashMap<>();
        for (int i = 0; i < subs.length; i++) {
            if (subs[i] != 0) {
                int length = 2 * subs[i];
                if (i != subs.length - 1) length += pals[i + 1];

                if (max <= length) {
                    max = length;
                    indexes.computeIfAbsent(max, empList -> new ArrayList<>()).add(i);
                }
            }
        }

        ArrayList<Integer> longest = indexes.get(max);
        if (longest != null) {
            String palindrome = null;
            for (int i = 0; i < longest.size(); i++) {
                int j = longest.get(i);
                String left = s1.substring(j - subs[j] + 1, j + 1);
                String mid = (j != s1.length() - 1 ? s1.substring(j + 1, j + 1 + pals[j + 1]) : "");
                String right = reverse(left);
                String next = left + mid + right;

                if (palindrome == null) palindrome = next;
                else if (next.compareTo(palindrome) < 0) palindrome = next;
            }

            return palindrome;
        } else {
            return "";
        }
    }

    private static String longestPalindrome_B_Search(String s1, String s2) {
        int[] subs = substringsLengthRight(s1, s2);
        int[] pals = PalindromicTree.buildTree(s1).getPalindromicArray();

        int max = 0;

        HashMap<Integer, ArrayList<Integer>> indexes = new HashMap<>();
        for (int i = 0; i < subs.length; i++) {
            if (subs[i] != 0) {
                int length = 2 * subs[i];
                if (i != 0) length += pals[i - 1];

                if (max <= length) {
                    max = length;
                    indexes.computeIfAbsent(max, empList -> new ArrayList<>()).add(i);
                }
            }
        }

        ArrayList<Integer> longest = indexes.get(max);
        if (longest != null) {
            String palindrome = null;
            for (int i = 0; i < longest.size(); i++) {
                int j = longest.get(i);
                String right = s1.substring(j, j + subs[j]);
                String mid = (j != 0 ? s1.substring(j - pals[j - 1], j) : "");
                String left = reverse(right);
                String next = left + mid + right;

                if (palindrome == null) palindrome = next;
                else if (next.compareTo(palindrome) < 0) palindrome = next;
            }

            return palindrome;
        } else {
            return "";
        }
    }

    static int[] substringsLengthLeft(String s1, String s2) {
        int[] right = substringsLengthRight(s1, s2);
        int[] left = new int[right.length];

        for (int i = right.length - 1; i >= 0; i--) {
            if (right[i] > 1) {
                if (i + right[i] - 1 < right.length) {
                    left[i + right[i] - 1] = right[i];
                    left[i] = -1;
                }
            } else {
                left[i] = right[i];
            }
        }

        int next = 1;
        for (int i = 0; i < left.length; i++) {
            if (left[i] == -1) left[i] = next++;
            else next = 1;
        }

        return left;
    }

    static int[] substringsLengthRight(String s1, String s2) {
        String s = s1 + reverse(s2);

        int[] codes = new int[s.length()];
        for (int i = 0; i < s.length(); i++)
            codes[i] = s.charAt(i) - 'a' + 1;

        int[] suff = SuffixArray.constructSuffixArray(codes, 26);
        int[] lcp = LCP.kasai(s, suff);
        System.out.println(Arrays.toString(lcp));

        int len = s1.length();
        int[] subs = new int[len];

        int min = 0;
        for (int i = 1; i < suff.length; i++) {
            if (suff[i - 1] < len || suff[i] < len) {
                if (suff[i - 1] >= len) {
                    subs[suff[i]] = lcp[i - 1];
                    min = lcp[i - 1];
                } else if (suff[i] >= len) {
                    subs[suff[i - 1]] = Math.max(subs[suff[i - 1]], lcp[i - 1]);
                } else {
                    min = Math.min(min, lcp[i - 1]);
                    subs[suff[i]] = min;
                }
            }
        }

        min = 0;
        for (int i = lcp.length - 1; i > 0; i--) {
            if (suff[i] >= len)
                min = lcp[i];

            min = Math.min(min, lcp[i - 1]);

            if (suff[i - 1] < len && suff[i] < len)
                subs[suff[i - 1]] = Math.max(subs[suff[i - 1]], min);
        }

        return subs;
    }

    public static String reverse(String s) {
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length / 2; i++) {
            char temp = chars[i];
            chars[i] = chars[chars.length - i - 1];
            chars[chars.length - i - 1] = temp;
        }

        return new String(chars);
    }
}
