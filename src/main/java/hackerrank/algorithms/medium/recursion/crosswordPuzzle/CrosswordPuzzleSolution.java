package hackerrank.algorithms.medium.recursion.crosswordPuzzle;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Hackerrank challenge.
 *
 * @see <a href="https://www.hackerrank.com/challenges/crossword-puzzle/problem">CrosswordPuzzle</a>
 *
 * @author Aleksandr Burkin
 */
public class CrosswordPuzzleSolution {

    private static char plus = '+';
    private static char minus = '-';

    private static class AnagramBuilder {

        static void createAnagram(int size, String[] words, ArrayList<String[]> anagrams) {
            if (size == 1)
                return;

            for (int i = 0; i < size; i++) {
                createAnagram(size - 1, words, anagrams);
                if (size == 2)
                    anagrams.add(Arrays.copyOf(words, words.length));

                rotate(words, size);
            }
        }

        static void rotate(String[] words, int size) {
            int i = words.length - size;
            String first = words[i];
            for (; i < words.length - 1; i++)
                words[i] = words[i + 1];

            words[i] = first;
        }
    }

    static String[] crosswordPuzzle(String[] crossword, String words) {
        ArrayList<String[]> anagrams = new ArrayList<>();
        String[] array = words.split(";");
        AnagramBuilder.createAnagram(array.length, array, anagrams);

        String[] result = new String[crossword.length];
        for (String[] anagram : anagrams) {
            char[][] charCrossword = new char[crossword.length][crossword[0].length()];
            for (int i = 0; i < charCrossword.length; i++)
                charCrossword[i] = crossword[i].toCharArray();

            if (tryFillCrossword(charCrossword, anagram)) {
                for (int i = 0; i < result.length; i++)
                    result[i] = new String(charCrossword[i]);

                break;
            }
        }

        return result;
    }

    private static boolean tryFillCrossword(char[][] crossword, String[] words) {
        int index = 0;
        for (int i = 0; i < crossword.length; i++) {
            for (int j = 0; j < crossword[i].length; j++) {
                if (crossword[i][j] == minus) {
                    String word = words[index++];

                    if (j != crossword[i].length - 1 && crossword[i][j + 1] == minus) {
                        if (!tryWordInsertHorizontally(crossword, word, i, j))
                            return false;
                    } else if (i != crossword.length - 1 && crossword[i + 1][j] == minus) {
                        if (!tryWordInsertVertically(crossword, word, i, j))
                            return false;
                    }
                }
            }
        }

        return index == words.length;
    }

    private static boolean tryWordInsertHorizontally(char[][] crossword, String word, int i, int j) {
        StringBuilder start = new StringBuilder();
        for (int k = j - 1; k >= 0 && crossword[i][k] != plus; k--)
            start.insert(0, crossword[i][k]);

        if (!word.startsWith(start.toString())) return false;

        String part = word.substring(start.length());

        int k = j;
        while (k < crossword[i].length && crossword[i][k] != plus && k - j != part.length()) {
            char symbol = crossword[i][k];
            if (symbol == minus)
                crossword[i][k] = part.charAt(k - j);
            else if (symbol != part.charAt(k - j))
                return false;

            k++;
        }

        return part.length() == k - j;
    }

    private static boolean tryWordInsertVertically(char[][] crossword, String word, int i, int j) {
        StringBuilder start = new StringBuilder();
        for (int k = i - 1; k >= 0 && crossword[k][j] != plus; k--)
            start.insert(0, crossword[k][j]);

        if (!word.startsWith(start.toString())) return false;

        String part = word.substring(start.length());

        int k = i;
        while (k < crossword.length && crossword[k][j] != plus && k - i != part.length()) {
            char symbol = crossword[k][j];
            if (symbol == minus)
                crossword[k][j] = part.charAt(k - i);
            else if (symbol != part.charAt(k - i))
                return false;

            k++;
        }

        return part.length() == k - i;
    }
}
