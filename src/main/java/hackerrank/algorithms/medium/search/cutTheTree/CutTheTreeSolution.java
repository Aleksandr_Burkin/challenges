package hackerrank.algorithms.medium.search.cutTheTree;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Hackerrank challenge.
 *
 * @see <a href="https://www.hackerrank.com/challenges/cut-the-tree/problem">CutTheTree</a>
 *
 * @author Aleksandr Burkin
 */
public class CutTheTreeSolution {

    private static int min;

    private static class Vertex {
        int data;
        ArrayList<Vertex> edges;
        boolean isVisited;

        Vertex(int data) {
            this.data = data;
            this.edges = new ArrayList<>();
        }

        void addEdge(Vertex child) {
            edges.add(child);
        }
    }

    static int solve(int[] data, int[][] edges) {
        HashMap<Integer, Vertex> vertices = new HashMap<>();

        int sum = 0;

        //build the tree
        for (int[] edge : edges) {
            Vertex v1 = vertices.get(edge[0]);
            Vertex v2 = vertices.get(edge[1]);

            if (v1 == null) {
                v1 = new Vertex(data[edge[0] - 1]);
                vertices.put(edge[0], v1);
                sum += v1.data;
            }
            if (v2 == null) {
                v2 = new Vertex(data[edge[1] - 1]);
                vertices.put(edge[1], v2);
                sum += v2.data;
            }

            v1.addEdge(v2);
            v2.addEdge(v1);
        }

        //find minimum difference
        min = sum;
        minDiff(vertices.get(1), sum);

        return min;
    }

    private static int minDiff(Vertex parent, int sum) {
        parent.isVisited = true;
        if (parent.edges.size() == 0) {
            checkMinDiff(sum, parent.data);
            return parent.data;
        }

        int subSum = parent.data;
        for (Vertex vertex : parent.edges)
            if (!vertex.isVisited)
                subSum += minDiff(vertex, sum);

        checkMinDiff(sum, subSum);

        return subSum;
    }

    private static void checkMinDiff(int sum, int subSum) {
        int topTreeSum = sum - subSum;
        int diff = Math.abs(topTreeSum - subSum);
        if (diff < min) min = diff;
    }
}
