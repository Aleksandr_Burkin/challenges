package hackerrank.algorithms.medium.search.gridlandMetro;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Hackerrank challenge.
 *
 * @see <a href="https://www.hackerrank.com/challenges/gridland-metro/problem">GridlandMetro</a>
 *
 * @author Aleksandr Burkin
 */
public class GridlandMetroSolution {

    private static class Interval {
        int start;
        int end;

        Interval(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }

    static long gridlandMetro(int r, int c, int k, int[][] track) {
        HashMap<Integer, ArrayList<Interval>> map = new HashMap<>();

        long result = (long)r * c;
        for (int i = 0; i < k; i++) {
            int row = track[i][0];
            int start = track[i][1];
            int end = track[i][2];

            ArrayList<Interval> tracks = map.get(row);
            if (tracks == null) {
                ArrayList<Interval> list = new ArrayList<>();
                list.add(new Interval(start, end));
                map.put(row, list);
            } else {
                boolean crossing = false;
                for (Interval value : tracks) {
                    if (start <= value.end || end <= value.start) {
                        if (start < value.start) value.start = start;
                        if (end > value.end) value.end = end;
                        crossing = true;
                        break;
                    }
                }
                if (!crossing) tracks.add(new Interval(start, end));
            }
        }

        for (ArrayList<Interval> list : map.values()) {
            for (Interval value : list) {
                result -= (value.end - value.start + 1);
            }
        }

        return result;
    }
}
