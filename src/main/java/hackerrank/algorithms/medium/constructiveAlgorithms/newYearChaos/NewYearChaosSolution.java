package hackerrank.algorithms.medium.constructiveAlgorithms.newYearChaos;

/**
 * Hackerrank challenge.
 *
 * @see <a href="https://www.hackerrank.com/challenges/new-year-chaos/problem">NewYearChaos</a>
 *
 * @author Aleksandr Burkin
 */
public class NewYearChaosSolution {

    static String minimumBribes(int[] a) {
        int result = 0;

        int steps = 0;
        for (int i = 0; i < a.length; i++) {
            if (i == a[i] - 1) {
                steps = 0;
            } else if (i >= a[i] - 3 - steps) {
                swap(a, i, i + steps + 1);
                steps++;
                i--;
                result++;
            } else {
                return "Too chaotic";
            }
        }

        return Integer.toString(result);
    }

    private static void swap(int[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }
}
