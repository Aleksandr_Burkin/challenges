package hackerrank.algorithms.medium.dynamicProgramming.redJohnIsBack;

import java.math.BigInteger;

/**
 * Hackerrank challenge.
 *
 * @see <a href="https://www.hackerrank.com/challenges/red-john-is-back/problem">RedJohnIsBack</a>
 *
 * @author Aleksandr Burkin
 */
public class RedJohnIsBackSolution {

    static int redJohn(int n) {
        int combs = calcCombinationsCount(n);

        return calcPrimesCount(combs);
    }

    private static BigInteger[] factorials(int n) {
        BigInteger[] factorials = new BigInteger[n + 1];
        factorials[0] = BigInteger.ONE;
        factorials[1] = BigInteger.ONE;

        for (int i = 2; i <= n; i++)
            factorials[i] = factorials[i - 1].multiply(BigInteger.valueOf(i));

        return factorials;
    }

    private static int calcCombinationsCount(int n) {
        int BLOCK_SIZE = 4;

        BigInteger[] factorials = factorials(n);

        int blocks = n / BLOCK_SIZE;
        int combs = 1;

        while (blocks > 0) {
            int singles = n - BLOCK_SIZE*blocks;

            combs += factorials[singles + blocks]
                    .divide(factorials[singles].multiply(factorials[blocks]))
                    .intValue();

            blocks--;
        }

        return combs;
    }

    private static int calcPrimesCount(int combs) {
        if (combs < 2) return 0;
        if (combs == 2) return 1;

        int[] primes = new int[combs + 1];
        primes[0] = 2;
        primes[1] = 3;
        int i = 1;
        while( i < primes.length - 1 ) {
            int cur = primes[i] + 2;
            int half = (int)Math.sqrt(cur);
            for (int j = 1; j <= i; j++) {
                if (cur % primes[j] == 0) {
                    cur += 2;
                    half = (int)Math.sqrt(cur);
                    j = 0;
                }
                if( primes[j] > half ) break;
            }
            if (cur > combs) break;

            primes[++i] = cur;
        }

        return i + 1;
    }
}
