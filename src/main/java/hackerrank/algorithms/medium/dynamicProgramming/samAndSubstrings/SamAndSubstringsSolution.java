package hackerrank.algorithms.medium.dynamicProgramming.samAndSubstrings;

/**
 * Hackerrank challenge.
 *
 * @see <a href="https://www.hackerrank.com/challenges/sam-and-substrings/problem">SamAndSubstrings</a>
 *
 * @author Aleksandr Burkin
 */
public class SamAndSubstringsSolution {

    private static final int MODULE = (int)(1e9 + 7);

    static int substrings(String s) {
        long sum = 0;
        long multiplier = 1;

        for (int i = s.length() - 1; i >= 0; i--) {
            int number = Integer.parseInt(s.charAt(i) + "");

            sum += (multiplier * number * (i + 1)) % MODULE;
            multiplier = (multiplier * 10 + 1) % MODULE;
        }

        return (int) (sum % MODULE);
    }
}
