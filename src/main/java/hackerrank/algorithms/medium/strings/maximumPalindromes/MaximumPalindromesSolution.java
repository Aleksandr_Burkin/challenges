package hackerrank.algorithms.medium.strings.maximumPalindromes;

import java.math.BigInteger;

/**
 * Hackerrank challenge.
 *
 * @see <a href="https://www.hackerrank.com/challenges/maximum-palindromes/problem">MaximumPalindromes</a>
 *
 * @author Aleksandr Burkin
 */
public class MaximumPalindromesSolution {

    private static int module = (int) (1e9 + 7);
    private static int[][] counter;
    private static long[] factorials;
    private static long[] inverseMod;
    private static BigInteger bigMod = BigInteger.valueOf(module);

    static void initialize(String s) {
        int alphabet = 26;

        counter = new int[s.length() + 1][alphabet];
        for (int i = 0; i < s.length(); i++) {
            System.arraycopy(counter[i], 0, counter[i + 1], 0, alphabet);
            counter[i + 1][s.charAt(i) - 'a']++;
        }

        factorials = new long[s.length() + 1];
        factorials[0] = 1;
        for (int i = 1; i <= s.length(); i++)
            factorials[i] = (factorials[i - 1] * i) % module;

        inverseMod = new long[s.length() + 1];
    }

    static long answerQuery(int l, int r) {
        int singles = 0;
        int pairs = 0;
        long divider = 1;

        for (int i = 0; i < 26; i++) {
            int count = counter[r][i] - counter[l - 1][i];
            int pair = count / 2;
            pairs += pair;
            if (pair > 0)
                divider = (divider * modInverse(pair)) % module;

            singles += count % 2;
        }

        long result = divider * factorials[pairs];
        if (singles > 0) result *= singles;

        return result % module;
    }

    static long modInverse(int pair) {
        if (inverseMod[pair] == 0)
            inverseMod[pair] = BigInteger.valueOf(factorials[pair]).modInverse(bigMod).longValue();

        return inverseMod[pair];
    }
}
