package hackerrank.algorithms.hard.search.bikeRacers;

/**
 * Hackerrank challenge.
 *
 * @see <a href="https://www.hackerrank.com/challenges/bike-racers/problem">BikeRacers</a>
 *
 * @author Aleksandr Burkin
 */
public class BikeRacersSolution {

    private static class GFG {
        private static int N;
        private static int M;

        private static boolean bpm(boolean[][] bpGraph, int u, boolean[] seen, int[] matchR) {
            for (int v = 0; v < M; v++) {
                if (bpGraph[u][v] && !seen[v]) {
                    seen[v] = true;

                    if (matchR[v] < 0 || bpm(bpGraph, matchR[v], seen, matchR)) {
                        matchR[v] = u;
                        return true;
                    }
                }
            }

            return false;
        }

        static int maxBPM(boolean[][] bpGraph) {
            int[] matchR = new int[M];

            for(int i = 0; i < M; i++)
                matchR[i] = -1;

            int result = 0;
            for (int u = 0; u < N; u++) {
                boolean[] seen = new boolean[M] ;
                for(int i = 0; i < M; i++)
                    seen[i] = false;

                if (bpm(bpGraph, u, seen, matchR))
                    result++;
            }
            return result;
        }
    }

    private static boolean match(long distance, long[][] dist, int N, int M, int K) {
        boolean[][] adj = new boolean[N][M];

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                //add edge
                if (dist[i][j] <= distance) adj[i][j] = true;
            }
        }

        int match = GFG.maxBPM(adj);

        return match >= K;
    }

    private static long euclidean(int[] d1, int[] d2) {
        return (long)(Math.pow(d1[0] - d2[0], 2) + Math.pow(d1[1] - d2[1], 2));
    }

    static long bikeRacers(int[][] bikers, int[][] bikes, int k) {
        long MAX = (long) 2e14;

        int N = bikers.length;
        int M = bikes.length;
        int K = k;

        GFG.N = N;
        GFG.M = M;

        long[][] dist = new long[N][M];

        for (int i = 0; i < bikers.length; i++)
            for (int j = 0; j < bikes.length; j++)
                dist[i][j] = euclidean(bikers[i], bikes[j]);

        long low = 0, high = MAX;
        long result = -1;

        while (low <= high) {
            long mid = (low + high) / 2;
            if (match(mid, dist, N, M, K)) {
                result = mid;
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }

        return result;
    }
}
