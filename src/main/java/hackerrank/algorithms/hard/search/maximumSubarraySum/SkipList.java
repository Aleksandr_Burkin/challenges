package hackerrank.algorithms.hard.search.maximumSubarraySum;

import java.util.Random;

public class SkipList<V extends Comparable<V>> {

    private Node head;
    private Random random;
    private long size;

    private long level() {
        long level = 0;

        while (level <= size && random.nextBoolean()) level++;

        return level;
    }

    public SkipList() {
        head = new Node(null, 0, null, null);
        random = new Random();
    }

    private class Node {
        V value;
        long level;
        Node next;
        Node down;

        Node(V value, long level, Node next, Node down) {
            this.value = value;
            this.level = level;
            this.next = next;
            this.down = down;
        }
    }

    public V ceiling(V value) {
        Node cur = head;
        Node ceiling = null;
        while (cur != null) {
            if (cur.next == null || cur.next.value.compareTo(value) > 0) {
                if (ceiling == null || (cur.next != null && ceiling.value.compareTo(cur.next.value)  > 0))
                    ceiling = cur.next;

                cur = cur.down;
                continue;
            } else if (cur.next.value.equals(value)) {
                return cur.next.value;
            }

            cur = cur.next;
        }

        return ceiling != null ? ceiling.value : null;
    }

    public void add(V value) {
        long level = level();
        if (level > head.level)
            head = new Node(null, level, null, head);

        Node cur = head;
        Node last = null;

        while (cur != null) {
            if (cur.next == null || cur.next.value.compareTo(value) > 0) {
                if (level >= cur.level) {
                    Node node = new Node(value, cur.level, cur.next, null);
                    if (last != null)
                        last.down = node;

                    cur.next = node;
                    last = node;
                }

                cur = cur.down;
                continue;
            } else if (cur.next.value.equals(value)) {
                cur.next.value = value;
                return;
            }

            cur = cur.next;
        }

        size++;
    }
}

