package hackerrank.algorithms.hard.search.maximumSubarraySum;

/**
 * Hackerrank challenge.
 *
 * @see <a href="https://www.hackerrank.com/challenges/maximum-subarray-sum/problem">MaximumSubarraySum</a>
 *
 * @author Aleksandr Burkin
 */
public class MaximumSubarraySumSolution {

    static long maximumSum(long[] a, long m) {
        long cur = 0, prev = 0, res = 0;
        SkipList<Long> skipList = new SkipList<>();

        for (int i = 0; i < a.length; i++) {
            cur = (prev + a[i]) % m;
            res = Math.max(res, cur);

            Long ceiling = skipList.ceiling(cur);

            if (ceiling != null)
                res = Math.max(res, (cur - ceiling + m) % m);

            skipList.add(cur);
            prev = cur;
        }

        return res;
    }
}
