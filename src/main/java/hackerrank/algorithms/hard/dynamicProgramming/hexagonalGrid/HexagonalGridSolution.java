package hackerrank.algorithms.hard.dynamicProgramming.hexagonalGrid;

/**
 * Hackerrank challenge.
 *
 * @see <a href="https://www.hackerrank.com/challenges/hexagonal-grid/problem">HexagonalGrid</a>
 *
 * @author Aleksandr Burkin
 */
public class HexagonalGridSolution {

    static String hexagonalGrid(String a, String b) {
        int[] snake = new int[a.length() + b.length()];

        int index = 0;
        for (int i = 0; i < a.length(); i++) {
            snake[index++] = a.charAt(i) - '0';
            snake[index++] = b.charAt(i) - '0';
        }

        int count = 0;
        int last = snake.length - 1;
        for (int i = 0; i <= last; i++) {
            if (snake[i] == 0) {
                count++;
            } else if (count % 2 != 0) {
                if (i < last && snake[i + 1] != 0) return "NO";
                else count = -1;
            } else {
                count = 0;
            }
        }

        if (count % 2 != 0) return "NO";
        else return "YES";
    }
}
