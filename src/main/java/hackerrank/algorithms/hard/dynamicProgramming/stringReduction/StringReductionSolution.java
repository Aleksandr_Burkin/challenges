package hackerrank.algorithms.hard.dynamicProgramming.stringReduction;

/**
 * Hackerrank challenge.
 *
 * @see <a href="https://www.hackerrank.com/challenges/string-reduction/problem">StringReduction</a>
 *
 * @author Aleksandr Burkin
 */
public class StringReductionSolution {

    static int stringReduction(String s) {
        int[] count = new int[3];

        boolean allSame = true;
        for (int i = 0; i < s.length(); i++) {
            count[s.charAt(i) - 'a']++;
            if (i != 0 && s.charAt(i) != s.charAt(i - 1))
                allSame = false;
        }

        int even = 0, odd = 0;
        for (int i = 0; i < count.length; i++)
            if (count[i] % 2 == 0) even++;
            else odd++;

        if (allSame) return s.length();
        else if (even == 0 || odd == 0) return 2;
        else return 1;
    }
}
