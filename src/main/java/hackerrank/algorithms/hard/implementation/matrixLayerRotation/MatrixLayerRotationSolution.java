package hackerrank.algorithms.hard.implementation.matrixLayerRotation;

import java.util.Arrays;

/**
 * Hackerrank challenge.
 *
 * @see <a href="https://www.hackerrank.com/challenges/matrix-rotation-algo/problem">MatrixLayerRotation</a>
 *
 * @author Aleksandr Burkin
 */
public class MatrixLayerRotationSolution {

    static void matrixRotation(int[][] matrix, int r) {
        int layer = 0;
        int row = matrix.length - 2 * layer;
        int col = matrix[0].length - 2 * layer;

        while (row >= 2 && col >= 2) {
            int[] array = toOneDimensionArray(matrix, row, col, layer);
            rotateLeft(array, r);
            intoMatrix(matrix, array, layer, row, col);

            layer++;
            row = matrix.length - 2 * layer;
            col = matrix[0].length - 2 * layer;
        }
    }

    private static int[] toOneDimensionArray(int[][] matrix, int row, int col, int layer) {
        int pos = 0;

        int[] array = new int[2 * row + 2 * (col - 2)];

        for (int i = layer; i < col + layer; i++)
            array[pos++] = matrix[layer][i];

        for (int i = layer + 1; i < row + layer - 1; i++)
            array[pos++] = matrix[i][col + layer - 1];

        for (int i = col + layer - 1; i >= layer; i--)
            array[pos++] = matrix[row + layer - 1][i];

        for (int i = row + layer - 2; i >= layer + 1; i--)
            array[pos++] = matrix[i][layer];

        return array;
    }

    private static void rotateLeft(int[] a, int k) {
        k %= a.length;

        int[] b = Arrays.copyOf(a, a.length);

        for (int i = 0; i < a.length; i++) {
            int pos = i - k;
            pos = pos < 0 ? a.length - Math.abs(pos) : pos;
            a[pos] = b[i];
        }
    }

    private static void intoMatrix(int[][] matrix, int[] array, int layer, int row, int col) {
        int pos = 0;

        for (int i = layer; i < col + layer; i++)
            matrix[layer][i] = array[pos++];

        for (int i = layer + 1; i < row + layer - 1; i++)
            matrix[i][col + layer - 1] = array[pos++];

        for (int i = col + layer - 1; i >= layer; i--)
            matrix[row + layer - 1][i] = array[pos++];

        for (int i = row + layer - 2; i >= layer + 1; i--)
            matrix[i][layer] = array[pos++];
    }
}
