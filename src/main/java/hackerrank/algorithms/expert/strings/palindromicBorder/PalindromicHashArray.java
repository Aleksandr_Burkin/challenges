package hackerrank.algorithms.expert.strings.palindromicBorder;

import java.util.ArrayList;
import java.util.TreeMap;

/**
 * This data structure is used instead of HashMap.
 * HashMap has a high overhead when hashing palindrome string.
 *
 * For example.
 * There is string "afaafa"
 * When method HashMap.put("afaafa", 1) is called HashMap calculated hashCode in linear time,
 * however PalindromicHashArray will use hashCode of substring "faaf" calculated earlier
 * what is more effective.
 *
 * @author Aleksandr Burkin
 */
public class PalindromicHashArray {

    private String str;

    //Integer - length
    private TreeMap<Integer, Hash>[] auxForest;
    //Integer - count
    private TreeMap<Hash, MutableInt>[] hashForest;

    public PalindromicHashArray(String str) {
        if (str == null || str.isEmpty())
            throw new IllegalArgumentException("String cannot be empty");

        this.str = str;

        Hash hashObj = new Hash(0, 0, 1, subhash(0, 0, 0));

        this.auxForest = new TreeMap[str.length()];
        this.auxForest[0] = new TreeMap<>();
        this.auxForest[0].put(1, hashObj);

        this.hashForest = new TreeMap[str.length()];
        this.hashForest[hashObj.hash] = new TreeMap<>();
        this.hashForest[hashObj.hash].put(hashObj, new MutableInt());
    }

    //Palindromic hash
    private class Hash implements Comparable<Hash> {
        private int low;
        private int high;
        private int length;
        private int hash;

        Hash(int low, int high, int length, int hash) {
            this.low = low;
            this.high = high;
            this.length = length;
            this.hash = hash;
        }

        @Override
        public int compareTo(Hash hash) {
            if (length != hash.length)
                return length - hash.length;

            int i = low, j = hash.high;
            int mid = length / 2;
            for (int k = 0; k < mid; k++) {
                if (str.charAt(i) != str.charAt(j))
                    return str.charAt(i) - str.charAt(j);

                i++; j--;
            }

            return 0;
        }
    }

    //for optimization TreeMap
    private class MutableInt {
        private int value = 1;

        void increment() {
            value++;
        }
    }

    public void hashPalindrome(int low, int high) {
        if (high == 0) return;

        Hash max = null;
        TreeMap<Integer, Hash> tree = auxForest[high - 1];

        if (low < high && tree != null) {
            Integer key = tree.floorKey(high - low - 1);
            if (key != null)
                max = tree.get(key);
        }

        int hash = 0;

        if (max != null) {
            hash = subhash(max.hash, low, max.low - 1);
            hash = subhash(hash, high, high);
        } else {
            hash = subhash(hash, low, high);
        }

        Hash hashObj = new Hash(low, high, high - low + 1, hash);

        if (auxForest[high] == null)
            auxForest[high] = new TreeMap<>();

        auxForest[high].put(hashObj.length, hashObj);

        if (hashForest[hash] == null) {
            hashForest[hash] = new TreeMap<>();
            hashForest[hash].put(hashObj, new MutableInt());
        } else {
            MutableInt value = hashForest[hash].get(hashObj);
            if (value == null) hashForest[hash].put(hashObj, new MutableInt());
            else value.increment();
        }
    }

    private int subhash(int hash, int low, int high) {
        int result = hash;

        for (int i = low; i <= high; i++)
            result = (31 * result + str.charAt(i)) % str.length();

        return result;
    }

    public ArrayList<Integer> getPalindromesCount() {
        ArrayList<Integer> list = new ArrayList<>();

        for (int i = 0; i < hashForest.length; i++) {
            TreeMap<Hash, MutableInt> tree = hashForest[i];
            if (tree != null) {
                for (MutableInt elem : tree.values()) {
                    list.add(elem.value);
                }
            }
        }

        return list;
    }
}
