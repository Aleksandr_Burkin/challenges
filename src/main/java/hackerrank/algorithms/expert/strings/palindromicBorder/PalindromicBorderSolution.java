package hackerrank.algorithms.expert.strings.palindromicBorder;

/**
 * Hackerrank challenge.
 *
 * @see <a href="https://www.hackerrank.com/challenges/palindromic-border/problem">PalindromicBorder</a>
 *
 * @author Aleksandr Burkin
 */
public class PalindromicBorderSolution {

    static int palindromicBorder(String s) {
        return PalindromicTree.buildTree(s).getBorders();
    }
}
