package hackerrank.algorithms.expert.strings.palindromicBorder;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * PalindromicTree and palindromic border algorithm in O(n) time
 *
 * @author Aleksandr Burkin
 */
public class PalindromicTree {

    private static int max = (int) (1e5 + 1);
    private static int module = (int) (1e9 + 7);

    private String str;

    private HashMap<Integer, Node> tree;

    private int current;

    //Index to store next unique palindrome
    private int next;

    //optimization
    private int[] dpIS;

    //identical strings length
    private int[] isl;

    //string of identical characters
    private StringBuilder identical;

    //count of identical strings
    private int[][] count;

    private PalindromicHashArray hashArray;

    private long borders;

    private class Node {
        int low, high;

        int length;

        int[] edges = new int[26];

        int suffixEdge;
    }

    private PalindromicTree(String str) {
        this.str = str;

        Node imaginary = new Node();
        imaginary.length = -1;
        imaginary.suffixEdge = 1;

        Node empty = new Node();
        empty.length = 0;
        empty.suffixEdge = 1;

        tree = new HashMap<>();
        tree.put(1, imaginary);
        tree.put(2, empty);

        next = 2;
        current = 1;

        dpIS = new int[max];
        dpIS[2] = 1;
        for (int i = 3; i < dpIS.length; i++) {
            dpIS[i] = (int)(((long)i * (i - 1) / 2 + dpIS[i - 1]) % module);
        }

        isl = new int[this.str.length()];
        identical = new StringBuilder();

        count = new int[8][max];

        hashArray = new PalindromicHashArray(str);
    }

    private int index(int i) {
        return str.charAt(i) - 'a';
    }

    private int searchParent(int edge, int i) {
        int parent = edge;

        int length = tree.get(parent).length;

        while ((i - length < 1) || (str.charAt(i) != str.charAt(i - length - 1))) {
            parent = tree.get(parent).suffixEdge;
            length = tree.get(parent).length;
        }

        return parent;
    }

    private void createPalindromeNode(int i, int parent) {
        next++;

        tree.get(parent).edges[index(i)] = next;

        Node node = tree.computeIfAbsent(next, arg -> new Node());
        node.length = tree.get(parent).length + 2;
        node.low = i - tree.get(next).length + 1;
        node.high = i;

        if (tree.get(next).length == 1) {
            tree.get(next).suffixEdge = 2;
        } else {
            parent = searchParent(tree.get(parent).suffixEdge, i);
            tree.get(next).suffixEdge = tree.get(parent).edges[index(i)];
        }

        current = next;

        refreshBorder(next, i);
    }

    private void insert(int i) {
        int parent = searchParent(current, i);

        //if palindrome found
        if (tree.get(parent).edges[index(i)] != 0) {
            current = tree.get(parent).edges[index(i)];

            refreshBorder(current, i);
        } else {
            createPalindromeNode(i, parent);
        }
    }

    private void refreshBorder(int index, int i) {
        int j = index;

        Node node = tree.get(j);
        if (i != str.length() - 1 && node.high - node.low + 1 == isl[node.high]) {
            if (i != 0 && identical.charAt(0) != str.charAt(node.high))
                calcIdentical();

            identical.append(str.charAt(node.high));
        } else {
            if (identical.charAt(0) != str.charAt(node.high))
                calcIdentical();

            while (j > 2) {
                Node current = tree.get(j);

                if (current.high - current.low + 1 != isl[current.high]) {
                    hashArray.hashPalindrome(current.low, current.high);

                    j = current.suffixEdge;
                } else {
                    identical = new StringBuilder(str.substring(current.low, current.high + 1));
                    break;
                }
            }
        }

        if (i == str.length() - 1)
            calcIdentical();
    }

    private void calcIdentical() {
        int letter = identical.charAt(0) - 'a';
        for (int i = identical.length(); i > 0; i--) {
            int subs = identical.length() - i + 1;
            borders = (borders + subs * count[letter][i]) % module;
            count[letter][i] += subs;
        }

        borders = (borders + dpIS[identical.length()]) % module;

        identical = new StringBuilder();
    }

    public static PalindromicTree buildTree(String str) {
        PalindromicTree pt = new PalindromicTree(str);

        for (int i = 0; i < str.length(); i++) {
            if (i > 0 && str.charAt(i) == str.charAt(i - 1))
                pt.isl[i] = pt.isl[i - 1] + 1;
            else
                pt.isl[i] = 1;

            pt.insert(i);
        }

        ArrayList<Integer> list = pt.hashArray.getPalindromesCount();
        for (Integer value : list) {
            pt.borders = (pt.borders + value * (value - 1) / 2) % module;
        }

        return pt;
    }

    public int getBorders() {
        return (int)borders;
    }
}
