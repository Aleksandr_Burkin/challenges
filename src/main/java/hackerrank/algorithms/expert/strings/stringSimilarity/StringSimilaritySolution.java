package hackerrank.algorithms.expert.strings.stringSimilarity;

/**
 * Hackerrank challenge.
 *
 * @see <a href="https://www.hackerrank.com/challenges/string-similarity/problem">StringSimilarity</a>
 *
 * @author Aleksandr Burkin
 */
public class StringSimilaritySolution {

    static long stringSimilarity(String s) {
        int[] z = Zfunction.zFunction(s);
        long result = s.length();
        for (int i = 0; i < z.length; i++) {
            result += z[i];
        }

        return result;
    }
}
