package hackerrank.dataStructures.hard.trie.noPrefixSet;

/**
 * Hackerrank challenge.
 *
 * @see <a href="https://www.hackerrank.com/challenges/no-prefix-set/problem">NoPrefixSet</a>
 *
 * @author Aleksandr Burkin
 */
public class NoPrefixSetSolution {

    private static final String GOOD_SET = "GOOD SET";
    private static final String BAD_SET = "BAD SET";
    private static final Integer DUMMY = 1;

    private static String bad = null;

    static class Trie {

        private static final int R = 26;
        private Node root;

        private static class Node {
            private Integer value;
            private Node[] next = new Node[R];
        }

        public void put(String key) {
            root = put(root, key, 0);
        }

        private Node put(Node x, String key, int d) {
            if (x != null && (d == key.length() || x.value != null)) bad = key;

            if (x == null) x = new Node();

            if (d == key.length()) {
                x.value = DUMMY;
                return x;
            }

            int pos = key.charAt(d) - 'a';
            x.next[pos] = put(x.next[pos], key, d + 1);

            return x;
        }
    }

    static String[] matchPrefix(String[] strings) {
        bad = null;

        Trie trie = new Trie();

        for (String s : strings) {
            trie.put(s);
            if (bad != null) return new String[] {BAD_SET, bad};
        }

        return new String[] {GOOD_SET};
    }
}
