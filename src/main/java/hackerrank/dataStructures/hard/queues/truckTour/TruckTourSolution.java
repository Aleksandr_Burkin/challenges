package hackerrank.dataStructures.hard.queues.truckTour;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Hackerrank challenge.
 *
 * @see <a href="https://www.hackerrank.com/challenges/truck-tour/problem">TruckTour</a>
 *
 * @author Aleksandr Burkin
 */
public class TruckTourSolution {

    static class Triple {
        int index;
        long petrol;
        long distance;

        Triple(int index, long petrol, long distance) {
            this.index = index;
            this.petrol = petrol;
            this.distance = distance;
        }
    }

    static int truckTour(int[][] petrolpumps) {
        Queue<Triple> queue = new LinkedList<>();

        for (int i = 0; i < petrolpumps.length; i++) {
            queue.add(new Triple(i, petrolpumps[i][0], petrolpumps[i][1]));
        }

        int result = 0;
        long petrol = 0;
        long distance = 0;

        while (!queue.isEmpty()) {
            Triple triple = queue.poll();
            if (petrol == 0) result = triple.index;

            petrol += triple.petrol;
            distance += triple.distance;

            if (petrol < distance) {
                queue.add(new Triple(-1, petrol, distance));
                petrol = 0;
                distance = 0;
            }
        }

        return result;
    }
}
