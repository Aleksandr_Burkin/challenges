package hackerrank.dataStructures.hard.queues.queriesWithFixedLength;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Hackerrank challenge.
 *
 * @see <a href="https://www.hackerrank.com/challenges/queries-with-fixed-length/problem">QueriesWithFixedLength</a>
 *
 * @author Aleksandr Burkin
 */
public class QueriesWithFixedLengthSolution {

    static int[] solve(int[] a, int[] queries) {
        int[] result = new int[queries.length];

        for (int i = 0; i < queries.length; i++) {
            int d = queries[i];

            int max = 0;
            Queue<Integer> queue = new LinkedList<>();
            for (int j = 0; j < d; j++) {
                queue.add(a[j]);
                max = Math.max(max, a[j]);
            }

            result[i] = max;

            for (int j = d; j < a.length; j++) {
                max = Math.max(max, a[j]);
                queue.add(a[j]);

                if (queue.poll() == max && !queue.isEmpty())
                    max = Collections.max(queue);

                result[i] = Math.min(result[i], max);
            }
        }

        return result;
    }
}
