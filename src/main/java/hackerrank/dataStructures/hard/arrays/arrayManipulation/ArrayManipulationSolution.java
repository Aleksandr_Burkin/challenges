package hackerrank.dataStructures.hard.arrays.arrayManipulation;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Hackerrank challenge.
 *
 * @see <a href="https://www.hackerrank.com/challenges/crush/problem">ArrayManipulation</a>
 *
 * @author Aleksandr Burkin
 */
public class ArrayManipulationSolution {

    static long arrayManipulation(int n, int[][] queries) {
        ArrayList<Integer>[] array = new ArrayList[n + 1];
        int[] storeQueries = new int[queries.length + 1];
        boolean[] isOpenQueries = new boolean[queries.length + 1];

        for (int i = 0; i < queries.length; i++) {
            int left = queries[i][0];
            int right = queries[i][1];
            int value = queries[i][2];

            if (array[left] == null) array[left] = new ArrayList<>();
            array[left].add(i + 1);

            if (array[right] == null) array[right] = new ArrayList<>();
            array[right].add(i + 1);

            storeQueries[i + 1] = value;
        }

        Queue<Integer> queue = new LinkedList<>();

        long max = 0;
        long current = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != null) {
                ArrayList<Integer> list = array[i];

                for (Integer query : list) {
                    if (!isOpenQueries[query]) {
                        current += storeQueries[query];
                        max = Math.max(max, current);

                        isOpenQueries[query] = true;
                    } else {
                        queue.add(query);
                    }
                }
                while (!queue.isEmpty()) {
                    current -= storeQueries[queue.remove()];
                }
            }
        }

        return max;
    }
}
