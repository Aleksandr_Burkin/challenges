package hackerrank.dataStructures.medium.stacks.gameOfTwoStacks;

import java.util.Stack;

/**
 * Hackerrank challenge.
 *
 * @see <a href="https://www.hackerrank.com/challenges/game-of-two-stacks/problem">GameOfTwoStacks</a>
 *
 * @author Aleksandr Burkin
 */
public class GameOfTwoStacksSolution {

    private static Stack<Integer> createStack(int[] values) {
        Stack<Integer> stack = new Stack<>();
        for (int i = values.length - 1; i >= 0; i--)
            stack.push(values[i]);

        return stack;
    }

    static int twoStacks(int x, int[] a, int[] b) {
        Stack<Integer> stackA = createStack(a);
        Stack<Integer> stackB = createStack(b);

        Stack<Integer> aux = new Stack<>();
        int sum = 0;
        int moves = 0;

        while (!stackA.isEmpty() && sum + stackA.peek() <= x) {
            int value = stackA.pop();
            sum += value;
            aux.push(value);
            moves++;
        }

        int max = moves;
        while (!aux.isEmpty() && !stackB.isEmpty()) {
            while (!aux.isEmpty() && sum + stackB.peek() > x) {
                sum -= aux.pop();
                moves--;
            }
            if (sum + stackB.peek() <= x) {
                sum += stackB.pop();
                moves++;
                if (moves > max) max = moves;
            }
        }

        while (!stackB.isEmpty() && sum + stackB.peek() <= x) {
            sum += stackB.pop();
            moves++;
        }

        return moves > max ? moves : max;
    }
}
